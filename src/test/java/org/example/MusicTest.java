package org.example;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

public class MusicTest {

    @Test
    public void testGoogle() throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\maksi\\Desktop\\chromedriver.exe");

        ChromeOptions options = new ChromeOptions();
        options.addArguments("--start-maximized");
        WebDriver driver = new ChromeDriver( options );

        driver.get("https://www.timex.com/");
        System.out.println("Page Title is " + driver.getTitle());

        // find search field
        WebElement findLine = driver.findElement(By.cssSelector("body #persistent-nav-banner #q"));
        findLine.sendKeys("Timex");

        // find search button and click
        findLine = driver.findElement(By.cssSelector("body #persistent-nav-banner fieldset button"));
        findLine.click();

        Thread.sleep(2000);

        // wait and click the 'refine' button to open the options window
        findLine = driver.findElement(By.cssSelector("body #wrapper #main #primary .search-result-options button"));
        findLine.click();

        Thread.sleep(1000);

        // select watch size
        findLine = driver.findElement(By.cssSelector("body #wrapper #main #primary .refinements-section .refinements fieldset .swatch-30_-_31mm label"));
        findLine.click();

        Thread.sleep(2000);

        // wait and click the 'refine' button to open the options window
        findLine = driver.findElement(By.cssSelector("body #wrapper #main #primary .search-result-options button"));
        findLine.click();

        Thread.sleep(1000);

        // select watch color
        findLine = driver.findElement(By.cssSelector("body #wrapper #main #primary .refinements-section .refinements fieldset .swatch-brown label"));
        findLine.click();

        Thread.sleep(1000);

        // select watch
        findLine = driver.findElement(By.cssSelector("body #wrapper #main #primary .fixed-width #search-result-items .product-tile  .thumb-link"));
        findLine.click();

        Thread.sleep(2000);

        // select specs
        findLine = driver.findElement(By.cssSelector("body #wrapper #main #primary #pdpMain .product-info .product-info-left .ui-tabs-nav #ui-id-2"));
        findLine.click();

        // check size
        findLine = driver.findElement(By.xpath("/html/body/div[2]/div[3]/div[3]/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]/div[1]/div[1]/div[1]/ul/li[1]/span[2]"));
        Assert.assertEquals("30 mm", findLine.getText());

        // check color
        findLine = driver.findElement(By.xpath("/html/body/div[2]/div[3]/div[3]/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]/div[1]/div[1]/div[1]/ul/li[3]/span[2]"));
        Assert.assertEquals("Brown", findLine.getText());

        driver.quit();
    }
}
