package org.example;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TestGoogle {

    @Test
    public void testGoogle() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\maksi\\Desktop\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.get("http://www.google.com");
        System.out.println("Page Title is " + driver.getTitle());
        Assert.assertEquals("Google", driver.getTitle());

        WebElement findLine = driver.findElement(By.xpath("/html/body/div[1]/div[3]/form/div[2]/div[1]/div[1]/div/div[2]/input"));
        findLine.sendKeys("SQR course is the best");

        WebElement searchButton = driver.findElement(By.xpath("/html/body/div[1]/div[3]/form/div[2]/div[1]/div[3]/center/input[1]"));
        searchButton.click();

        findLine = driver.findElement(By.xpath("/html/body/div[4]/form/div[2]/div[1]/div[2]/div/div[2]/input"));
        Assert.assertEquals("SQR course is the best", findLine.getAttribute("value"));

        driver.quit();
    }
}
